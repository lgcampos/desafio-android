package lucas.campos.desafio.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import lucas.campos.desafio.features.pullrequest.PullRequestActivity
import lucas.campos.desafio.features.repository.RepositoryActivity

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindRepositoryActivity(): RepositoryActivity

    @ContributesAndroidInjector
    abstract fun bindPullRequestActivity(): PullRequestActivity

}