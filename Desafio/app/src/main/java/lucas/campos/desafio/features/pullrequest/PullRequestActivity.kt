package lucas.campos.desafio.features.pullrequest

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.ViewFlipper
import kotlinx.android.synthetic.main.activity_pull_request.*
import lucas.campos.desafio.R
import lucas.campos.desafio.data.model.Repository
import lucas.campos.desafio.features.base.BaseActivity
import lucas.campos.desafio.features.base.BaseViewModel
import lucas.campos.desafio.features.repository.RepositoryActivity
import javax.inject.Inject

class PullRequestActivity : BaseActivity() {

    @Inject
    lateinit var factory: PullRequestViewModelFactory

    private val viewModel by lazy { ViewModelProviders.of(this, factory).get(PullRequestViewModel::class.java)  }

    private val repository: Repository by lazy {
        intent.getParcelableExtra<Repository>(RepositoryActivity.REPOSITORY_EXTRAS) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_request)

        pullRequests.layoutManager = LinearLayoutManager(this)

        setUpViewModel()
    }

    private fun setUpViewModel() {
        viewModel.apply {
            results.observe(this@PullRequestActivity,
                    Observer { pullRequests.adapter = PullRequestAdapter(it) })

            fetchPullRequests(repository.owner.login, repository.name)
        }
    }

    override fun getViewModel(): BaseViewModel = viewModel

    override fun getViewFlipper(): ViewFlipper = flipper

    override fun getRetryAction(): Nothing? = null

}
