package lucas.campos.desafio.features.repository

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.adapter_repository.view.*
import lucas.campos.desafio.R
import lucas.campos.desafio.data.model.Repository

class RepositoryAdapter(private val repositories: List<Repository>?, private val clickListener: Consumer<Repository>) : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_repository, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        repositories?.let {
            val repository = it[position]

            with(holder) {
                name.text = repository.name
                description.text = repository.description
                username.text = repository.owner.login
                stars.text = repository.watchers.toString()
                forks.text = repository.forks.toString()
                Glide.with(holder.itemView.context)
                        .load(repository.owner.avatar)
                        .apply(bitmapTransform(CircleCrop()))
                        .into(photo)

                itemView.setOnClickListener { clickListener.accept(repository) }
            }
        }
    }

    override fun getItemCount() = repositories?.size ?: 0

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val name = view.name
        val photo = view.photo
        val username = view.username
        val description = view.description
        val stars = view.stars
        val forks = view.forks
    }
}