package lucas.campos.desafio

import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import lucas.campos.desafio.di.DaggerChallengeComponent

open class ChallengeApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        setupLeakCanary()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerChallengeComponent.builder().create(this)
    }

    private fun setupLeakCanary() {
        if (!LeakCanary.isInAnalyzerProcess(this)) {
            LeakCanary.install(this)
        }
    }

    open fun getUrl() = BuildConfig.BASE_URL

}