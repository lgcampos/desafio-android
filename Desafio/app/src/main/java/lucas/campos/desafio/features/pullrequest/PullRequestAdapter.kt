package lucas.campos.desafio.features.pullrequest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import kotlinx.android.synthetic.main.adapter_pull_request.view.*
import lucas.campos.desafio.R
import lucas.campos.desafio.data.model.PullRequest

class PullRequestAdapter(private val pullRequests: List<PullRequest>?) : RecyclerView.Adapter<PullRequestAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_pull_request, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        pullRequests?.let {
            val pullRequest = it[position]

            with (holder) {
                title.text = pullRequest.title
                description.text = pullRequest.body
                username.text = pullRequest.user.login
                Glide.with(itemView.context)
                        .load(pullRequest.user.avatar)
                        .apply(bitmapTransform(CircleCrop()))
                        .into(avatar)
            }
        }
    }

    override fun getItemCount() = pullRequests?.size ?: 0

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.title
        val description = view.description
        val username = view.username
        val avatar = view.avatar
    }
}
