package lucas.campos.desafio.features.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {

    protected val disposable = CompositeDisposable()

    val isLoading = MutableLiveData<Boolean>()
    val error = MutableLiveData<Throwable>()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}
