package lucas.campos.desafio.features.pullrequest

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import lucas.campos.desafio.data.ChallengeApi
import lucas.campos.desafio.data.model.PullRequest
import lucas.campos.desafio.extension.handlerLoader
import lucas.campos.desafio.features.base.BaseViewModel
import javax.inject.Inject
import javax.inject.Singleton

class PullRequestViewModel(private val api: ChallengeApi) : BaseViewModel() {

    val results = MutableLiveData<List<PullRequest>>()

    fun fetchPullRequests(owner: String, repository: String) {
        if (results.value == null) {
            disposable.add(api.listPullRequest(owner, repository)
                    .handlerLoader(isLoading)
                    .subscribe(results::postValue , error::postValue))
        }
    }

}

@Singleton
class PullRequestViewModelFactory @Inject constructor(private var api: ChallengeApi): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel>create(modelClass: Class<T>) = PullRequestViewModel(api) as T

}