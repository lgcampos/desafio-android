package lucas.campos.desafio

import android.support.design.widget.Snackbar
import android.view.View
import retrofit2.HttpException
import java.net.UnknownHostException

fun snackbar(view: View, message: String, duration: Int = Snackbar.LENGTH_LONG, retryAction: (() -> Unit)? = null) {
    val snackbar = Snackbar.make(view, message, duration)

    retryAction?.let {
        snackbar.setAction("Tentar novamente", { it() })
        snackbar.duration = Snackbar.LENGTH_INDEFINITE
    }

    snackbar.show()
}

//TODO: improve
fun handlerRequestError(throwable: Throwable, view: View, retryAction: (() -> Unit)? = null) {
    when (throwable) {
        is HttpException -> {
            when (throwable.code()) {
                500 -> snackbar(view, "Falha no servidor, tenta mais tarde!", retryAction = retryAction)

            }
        }
        is UnknownHostException -> {
            snackbar(view, "Sem conexão", retryAction = retryAction)
        }
    }
}