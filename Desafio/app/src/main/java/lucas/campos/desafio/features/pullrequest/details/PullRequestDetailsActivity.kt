package lucas.campos.desafio.features.pullrequest.details

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import lucas.campos.desafio.R

class PullRequestDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_request_details)
    }
}