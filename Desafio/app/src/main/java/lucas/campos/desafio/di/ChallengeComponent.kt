package lucas.campos.desafio.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import lucas.campos.desafio.ChallengeApp
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, ChallengeModule::class, ActivityBuilder::class])
interface ChallengeComponent : AndroidInjector<ChallengeApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<ChallengeApp>()

}