package lucas.campos.desafio.extension

import android.arch.lifecycle.MutableLiveData
import io.reactivex.Single

fun <T> Single<T>.handlerLoader(isLoading: MutableLiveData<Boolean>) =
        this
                .doOnSubscribe { isLoading.postValue(true) }
                .doOnError { isLoading.postValue(false) }
                .doOnSuccess { isLoading.postValue(false) }
