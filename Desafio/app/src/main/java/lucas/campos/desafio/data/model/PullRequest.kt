package lucas.campos.desafio.data.model

class PullRequest(val title: String, val body: String, val user: Owner)