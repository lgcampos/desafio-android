package lucas.campos.desafio.features.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ViewFlipper
import dagger.android.AndroidInjection
import lucas.campos.desafio.handlerRequestError

private const val SHOW_LOADER = 1
private const val SHOW_CONTENT = 0

abstract class BaseActivity : AppCompatActivity()  {

    private val root: View by lazy { findViewById<View>(android.R.id.content) }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        getViewModel()?.let {
            it.isLoading.observe(this,
                Observer {
                    getViewFlipper()?.displayedChild = if (it == true) SHOW_LOADER else SHOW_CONTENT
                }
            )

            // TODO: implement single observer
            it.error.observe(this, Observer { handlerRequestError(it!!, root) })
        }
    }

    abstract fun getViewModel(): BaseViewModel?

    abstract fun getViewFlipper(): ViewFlipper?

    abstract fun getRetryAction(): (() -> Unit)?

}