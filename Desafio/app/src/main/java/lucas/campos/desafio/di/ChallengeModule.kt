package lucas.campos.desafio.di

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import lucas.campos.desafio.BuildConfig
import lucas.campos.desafio.ChallengeApp
import lucas.campos.desafio.data.ChallengeApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ChallengeModule {

    private val TIMEOUT_SECONDS = 30

    @Provides
    @Singleton
    fun provideApi(client: OkHttpClient, app: ChallengeApp) =
         Retrofit.Builder()
                .baseUrl(app.getUrl())
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create(ChallengeApi::class.java)

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
                .readTimeout(TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().setLevel(
                        if (BuildConfig.DEBUG)
                            HttpLoggingInterceptor.Level.BODY
                        else
                            HttpLoggingInterceptor.Level.NONE
                ))

        return client.build()
    }

}