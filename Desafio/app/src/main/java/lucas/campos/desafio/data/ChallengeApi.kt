package lucas.campos.desafio.data

import io.reactivex.Single
import lucas.campos.desafio.data.model.PullRequest
import lucas.campos.desafio.data.model.RepositoryWrapper
import retrofit2.http.GET
import retrofit2.http.Path

interface ChallengeApi {

    @GET("search/repositories?q=language:Java&sort=stars&page=1")
    fun listRepositories(): Single<RepositoryWrapper>

    @GET("repos/{owner}/{repository}/pulls")
    fun listPullRequest(@Path("owner") owner: String, @Path("repository") repository: String): Single<List<PullRequest>>

}