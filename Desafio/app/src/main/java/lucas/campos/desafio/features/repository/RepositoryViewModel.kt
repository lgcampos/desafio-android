package lucas.campos.desafio.features.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory
import lucas.campos.desafio.data.ChallengeApi
import lucas.campos.desafio.data.model.Repository
import lucas.campos.desafio.extension.handlerLoader
import lucas.campos.desafio.features.base.BaseViewModel
import javax.inject.Inject
import javax.inject.Singleton

class RepositoryViewModel(private val api: ChallengeApi) : BaseViewModel() {

    private val repositories = mutableListOf<Repository>()

    val results = MutableLiveData<List<Repository>>()

    fun fetchRepositories() {
        if (repositories.isEmpty()) {
            disposable.add(api.listRepositories()
                    .handlerLoader(isLoading)
                    .subscribe({
                        results.postValue(repositories.apply { addAll(it.items) }.toList())
                    }, error::postValue))
        } else {
            results.postValue(repositories)
        }
    }

}

@Singleton
class RepositoryViewModelFactory @Inject constructor(private var api: ChallengeApi): NewInstanceFactory() {

    override fun <T : ViewModel>create(modelClass: Class<T>) = RepositoryViewModel(api) as T

}