package lucas.campos.desafio.data.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class RepositoryWrapper(val items: List<Repository>)

@Parcelize
@SuppressLint("ParcelCreator")
class Repository(val name: String, val description: String, val forks: Int, val owner: Owner, val watchers: Int) : Parcelable

@Parcelize
@SuppressLint("ParcelCreator")
class Owner(val login: String, @SerializedName("avatar_url") val avatar: String) : Parcelable