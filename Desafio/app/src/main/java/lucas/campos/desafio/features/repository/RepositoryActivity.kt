package lucas.campos.desafio.features.repository

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.widget.ViewFlipper
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.activity_main.*
import lucas.campos.desafio.R
import lucas.campos.desafio.features.base.BaseActivity
import lucas.campos.desafio.features.base.BaseViewModel
import lucas.campos.desafio.features.pullrequest.PullRequestActivity
import javax.inject.Inject

class RepositoryActivity : BaseActivity() {

    @Inject
    lateinit var factory: RepositoryViewModelFactory

    private val viewModel by lazy { ViewModelProviders.of(this, factory).get(RepositoryViewModel::class.java) }

    private var layoutManagerState: Parcelable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        repositories.layoutManager = LinearLayoutManager(this)

        setUpViewModel()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(LAYOUT_MANAGER_STATE, repositories.layoutManager.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let {
            layoutManagerState = it.getParcelable(LAYOUT_MANAGER_STATE)
        }
    }

    private fun setUpViewModel() {
        viewModel.apply {
            val activityRef = this@RepositoryActivity

            results.observe(activityRef,
                    Observer {
                        repositories.adapter = RepositoryAdapter(it, Consumer {
                            startActivity(
                                    Intent(activityRef, PullRequestActivity::class.java)
                                            .putExtra(REPOSITORY_EXTRAS, it))
                        })

                        layoutManagerState.let {
                            repositories.layoutManager.onRestoreInstanceState(it)
                        }
                    })

            fetchRepositories()
        }
    }

    override fun getViewModel(): BaseViewModel = viewModel

    override fun getViewFlipper(): ViewFlipper = flipper

    override fun getRetryAction() = { viewModel.fetchRepositories() }

    companion object {
        const val REPOSITORY_EXTRAS = "repository"
        const val LAYOUT_MANAGER_STATE = "LAYOUT_MANAGER_STATE"
    }

}