package lucas.campos.desafio

import android.support.test.InstrumentationRegistry
import br.com.concretesolutions.requestmatcher.InstrumentedTestRequestMatcherRule
import org.junit.Before
import org.junit.Rule

abstract class BaseInstrumentedTest {

    @Rule
    @JvmField
    var serverRule = InstrumentedTestRequestMatcherRule()

    @Before
    fun setUp() {
        val app: ChallengeTestApp = InstrumentationRegistry.getTargetContext().applicationContext as ChallengeTestApp
        app.baseUrl = serverRule.url("/").toString()
    }
}