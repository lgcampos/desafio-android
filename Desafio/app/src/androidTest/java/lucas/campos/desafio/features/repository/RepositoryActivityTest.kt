package lucas.campos.desafio.features.repository

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import br.com.concretesolutions.requestmatcher.model.HttpMethod
import lucas.campos.desafio.BaseInstrumentedTest
import org.junit.Rule
import org.junit.Test

class RepositoryActivityTest : BaseInstrumentedTest() {

    @get:Rule
    var activityRule = ActivityTestRule<RepositoryActivity>(RepositoryActivity::class.java, true, false)

    @Test
    fun shouldDisplayRepositoriesForUserWhenStartScreen() {
        serverRule
                .addFixture(200, "list-repositories.json")
                .ifRequestMatches()
                .methodIs(HttpMethod.GET)

        activityRule.launchActivity(null)

        onView(withText("RxJava")).check(matches(isDisplayed()))
    }

}
