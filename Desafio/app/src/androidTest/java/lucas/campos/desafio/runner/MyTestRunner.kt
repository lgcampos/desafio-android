package lucas.campos.desafio.runner

import android.app.Application
import android.content.Context
import android.support.test.runner.AndroidJUnitRunner
import lucas.campos.desafio.ChallengeTestApp

class MyTestRunner : AndroidJUnitRunner() {

    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, ChallengeTestApp::class.java.name, context)
    }

}