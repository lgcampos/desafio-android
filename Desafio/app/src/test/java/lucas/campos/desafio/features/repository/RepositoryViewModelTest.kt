package lucas.campos.desafio.features.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import io.reactivex.Single
import lucas.campos.desafio.data.ChallengeApi
import lucas.campos.desafio.data.model.Owner
import lucas.campos.desafio.data.model.Repository
import lucas.campos.desafio.data.model.RepositoryWrapper
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RepositoryViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock private lateinit var api: ChallengeApi
    @Mock private lateinit var repositories: Observer<MutableList<Repository>>
    @Mock private lateinit var error: Observer<Throwable>

    private lateinit var viewModel: RepositoryViewModel

    @Before
    fun setUp() {
        viewModel = RepositoryViewModel(api)
        viewModel.results.observeForever(repositories)
        viewModel.error.observeForever(error)
    }

    @Test
    fun shouldUpdateObserverWhenFetchedRepositories() {
        // given
        val repoWrapper = getRepoWrapper()
        `when`(api.listRepositories()).thenReturn(Single.just(repoWrapper))

        // when
        viewModel.fetchRepositories()

        // then
        verify(repositories).onChanged(repoWrapper.items.toMutableList())
    }

    @Test
    fun shouldNotifyErrorWhenSomethingUnexpectedHappen() {
        // given
        val throwable = Throwable()
        `when`(api.listRepositories()).thenReturn(Single.error(throwable))

        // when
        viewModel.fetchRepositories()

        // then
        verify(error).onChanged(throwable)
    }

    private fun getRepoWrapper() =
            RepositoryWrapper(mutableListOf(
                    Repository(
                            "name",
                            "description",
                            15,
                            Owner(
                                    "username",
                                    "imageUrl"),
                            30)
            ))
}

