package lucas.campos.desafio.extension

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.InOrder
import org.mockito.Mock
import org.mockito.Mockito.inOrder
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RxExtensionKtTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<Boolean>
    private lateinit var inOrder: InOrder
    private var isLoading = MutableLiveData<Boolean>()

    @Before
    fun setUp() {
        isLoading.observeForever(observer)
        inOrder = inOrder(observer)
    }

    @Test
    fun shouldShowAndHideLoaderInSuccessfulFlow() {
        // given
        val single = Single.just(0)

        // when
        single.handlerLoader(isLoading).subscribe()

        // then
        inOrder.verify(observer).onChanged(true)
        inOrder.verify(observer).onChanged(false)
    }

    @Test
    fun shouldShowAndHideLoaderInErrorFlow() {
        // given
        val single = Single.error<Throwable>(Throwable())

        // when
        single.handlerLoader(isLoading).subscribe()

        // then
        inOrder.verify(observer).onChanged(true)
        inOrder.verify(observer).onChanged(false)
    }
}